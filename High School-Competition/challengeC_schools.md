![NUST Logo](../images/nust.png)

Challenge for High School Teams
============================

<p style='text-align: justify; font-size: 1.2em;'><b>Problem C: Multiple Choice and T/F  </b><br><br/>Mrs. Gawases often gives <b>multiple choice and True/False(T/F)</b> quizzes in her classroom. She likes this since she is able to feed the forms from the quizzes into a specialized scanner to automatically grade all of the quizzes. 
<br/><br/>However, this term, Mrs. Gawases’  class is being held in a computer lab, and all of the learners will have access to a computer during the quizzes. She created a <b>website</b> in which the learners can input their multiple choice answers, and has the learners’ answers download to her PC for grading. 
  <br/><br/>Your tasked to help Mrs. Gawases out by writing this program which reads in this input, creating a grade for each learner based on their answers, and creating sorted results on output.  </p>



<p style='text-align: justify; font-size: 1.2em;'><b>Input </b>  <br/>
Input starts with a single integer on its own line, 𝑁 (1≤𝑁≤40), which indicates the number of questions on the quiz. The next 𝑁 lines indicate the answer key, each line with the capital letter A, B, C, or D for multiple choice and T or F for True or False quizzes on it. 
<br/><br/>After reading the answer key, you will read a single integer on its own line, 𝑀 (1≤𝑀≤300), which indicates the number of learners who took the quiz. Then, for each of the 𝑀 learners comes: 
<br/><ul>
  <li>A line with an integer 𝑆 (1≤𝑆≤109) on its own, indicating the student ID of that particular student. You are guaranteed that 𝑆 will be unique for each learner in a particular input.</li>
  <li>𝑁 lines indicating the learner’s answers. You are guaranteed each line will only have the capital letter A, B, C, or D otherwise only T or F on it.</li>
</ul> 
<br/>Finally, the last line of input will contain <b>LEARNER_ID_ASC, LEARNER _ID_DESC, GRADE_ASC, or GRADE_DESC </b>, indicating the order in which you should output the results. These values indicate sort by Learner ID (ascending or descending), or by grade (ascending or descending) respectively. When two or more learners have the same number of correct answers, you should order those Learners by Learner ID, ascending.  </p>

<p style='text-align: justify; font-size: 1.2em;'><b>Output </b>  <br/>
Output the Learner ID and grade (an integer with the number of correct answers) for each learner on its own line, in the order specified by the input. </p>



<p style='text-align: justify; font-size: 1.2em;'>
<b>Sample Input 1&emsp;&emsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Sample Output 1 </b>
 <br/>5&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;1234 5 
 <br/>A&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;800 2 
 <br/>C&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;4321 2 
 <br/>D 
 <br/>C 
 <br/>B 
 <br/>3 
 <br/>1234 
 <br/>A 
 <br/>C 
 <br/>D 
 <br/>C 
 <br/>B 
 <br/>4321 
 <br/>A 
 <br/>C 
 <br/>C 
 <br/>B 
 <br/>D 
 <br/>800 
 <br/>B 
 <br/>D 
 <br/>D 
 <br/>C 
 <br/>D 
 <br/>GRADE_DESC
</p>  

<p style='text-align: justify; font-size: 1.2em;'>
<b>Sample Input 2&emsp;&emsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Sample Output 2 </b>
 <br/>4&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;1300 3  
 <br/>T&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;1500 2 
 <br/>T 
 <br/>F
 <br/>T 
 <br/>2 
 <br/>1500
 <br/>T
 <br/>F 
 <br/>T
 <br/>T 
 <br/>1300
 <br/>F
 <br/>T 
 <br/>F 
 <br/>T  
 <br/>LEARNER_ID_ASC
</p> 









